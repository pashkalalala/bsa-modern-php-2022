<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    public array $cars = [];
    
    public function __construct(
        public float $lapLength,
        public int $lapsNumber)
    { }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $totalDistance = $this->lapLength * $this->lapsNumber;
        $finallyTimes = [];
    
        foreach ($this->all() as $car) {
            $driveTime = $totalDistance / $car->getSpeed();
            $pitStopTimes = (($totalDistance / 100) / ($car->getFuelTankVolume() / $car->getFuelConsumption()));
            $finallyTimes[] = $driveTime * 3600 + ($pitStopTimes * $car->getPitStopTime());
        }
    
        return $this->cars[array_keys($finallyTimes, min($finallyTimes))[0]];
    }
}