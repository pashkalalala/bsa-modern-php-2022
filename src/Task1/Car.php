<?php

declare(strict_types=1);

namespace App\Task1;

class Car
{
    public function __construct(
        public int $id,
        public string $image,
        public string $name,
        public int $speed,
        public int $pitStopTime,
        public float $fuelConsumption,
        public float $fuelTankVolume
    ) {
    
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }
}